# Valgrind on MPI programs

## Launching valgrind

To launch a `valgrind` on an MPI program, run the following

```sh
srun -N 4 -n 20 valgrind --tool=memcheck --log-file='memcheck_rank_%q{SLURM_PROCID}_proc_%p.vg' --suppressions=MPI_suppressions.supp <MPI program> <args>
```

The environment variable `$SLURM_PROCID` corresponds to the rank of the
process. The special characters `%p` are replaced with the PID of the process.

We can suppress memory leaks and errors ocurring in MPI library calls (which
can't be fixed) by specifying a 'suppressions file'. See below for more on
defining the suppressions file.


## Creating Suppressions

Valgrind can automatically generate suppressions by running:
```sh
srun -N 1 -n 20 valgrind --gen-suppressions=yes <MPI program>
```
Try running `valgrind` on a simple (e.g. 'hello world') MPI program to generate
suppression for common initialization/finalization errors.


### Examples

An example supressions file `ompi.supp` is included in this directory. This
file is specifically tailored to `OpenMPI` 3.1.3 built with the Intel compiler
18.0.3. The file was generated using the `valgrind --gen-suppressions=yes`
method described above.
