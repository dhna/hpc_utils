# HPC Utils
A collection of useful utilities for HPC.

## Usage

The following utilities and reference documents are included:
- `xthi.x`: Reports the thread affinity for MPI ranks and OpenMP threads. Requires and MPI C compiler to build.
- `deviceQuery.x`: Prints detailed information about all Nvidia GPUs on the machine. Requires the `nvcc` compiler to build.
- `./valgrind`: Information on how to run MPI-parallel valgrind.

To compile the executables, simply run `make` from the root directory.

## Thread Affinity Examples

To run 4 MPI ranks, each with 4 OpenMP threads, spread evenly throughout a single node:
```sh
export OMP_NUM_THREADS=4
export OMP_PLACES=cores
export OMP_PROC_BIND=close
mpiexec --report-bindings -n 4 --map-by socket:PE=4 xthi.x
```
The flag `--map-by socket:PE=4` spreads ranks among sockets and reserves 4 cpus per rank.

The same result can be achieved using `srun`, as follows:
```sh
export OMP_NUM_THREADS=4
export OMP_PLACES=cores
export OMP_PROC_BIND=close
srun -N 1 --ntasks=4 --cpus-per-task=4 --cpu-bind=core xthi.x 
```
The `srun` flag `--cpu-bind=core` binds ranks to core, while `--cpus-per-task` reserves 4 cpus per rank.
