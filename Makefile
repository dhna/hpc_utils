MPICC = mpicc
NVCC = nvcc
CFLAGS = -c
OMP_FLAGS = -qopenmp

SRCS_C = $(wildcard *.c)
SRCS_CU = $(wildcard *.cu)
EXE = $(SRCS_C:%.c=%.x) \
	  $(SRCS_CU:%.cu=%.x)

all: $(EXE)

debug: CFLAGS += -g -DDEBUG
debug: all

%.x: %.c
	$(MPICC) $(OMP_FLAGS) $< -o $@

%.x: %.cu
	$(NVCC) $< -o $@

clean:
	rm -rf *.o $(EXE) ~*

.PHONY: clean

